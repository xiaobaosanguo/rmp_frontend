/* eslint-disable no-nested-ternary */

'use client';

import Image from 'next/image';
import React, { useState } from 'react';

const RateBox = ({
  value,
  setValue,
  imageUrl,
  title,
}: {
  value: number;
  setValue: (value: number) => void;
  imageUrl: string;
  title: string;
}) => {
  // const router = useRouter();
  const [hoveredSvg, setHoveredSvg] = useState(0);

  return (
    <div className="mb-[20px] min-w-[300px] text-left text-green-400">
      <div className="divide-solid rounded-[40px] border-[1px] border-green-800 bg-green-50 px-[28px] pb-[20px] pt-[16px] text-left shadow-lg">
        <div className="flex flex-col text-left">
          <div className="mb-[10px] flex flex-row items-center text-[16px] font-bold">
            <Image src={imageUrl} alt="rating1" width={50} height={50} />
            <span className="ml-2">{title}</span>
          </div>

          <div className="flex flex-col items-center space-x-4 text-[14px]">
            <div className="flex flex-row justify-center">
              <svg
                style={{
                  color:
                    value >= 1
                      ? '#F83225'
                      : hoveredSvg >= 1
                        ? '#F83225'
                        : '#cbd5e0',

                  opacity:
                    (value >= 1 && hoveredSvg >= 1) || value < 1 ? 0.5 : 1,
                }}
                className="ms-1 size-[40px] transition-colors delay-0 duration-200 ease-in-out"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                viewBox="0 0 22 20"
                onMouseEnter={() => setHoveredSvg(1)}
                onMouseLeave={() => setHoveredSvg(0)}
                onClick={() => {
                  setValue(value === 1 ? 0 : 1);
                }}
              >
                <path d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
              </svg>
              <svg
                style={{
                  color:
                    value >= 2
                      ? '#F87125'
                      : hoveredSvg >= 2
                        ? '#F87125'
                        : '#cbd5e0',
                  opacity:
                    (value >= 2 && hoveredSvg >= 2) || value < 2 ? 0.5 : 1,
                }}
                className="ms-1 size-[40px] opacity-50 transition-colors delay-0 duration-200 ease-in-out"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                viewBox="0 0 22 20"
                onMouseEnter={() => setHoveredSvg(2)}
                onMouseLeave={() => setHoveredSvg(0)}
                onClick={() => {
                  setValue(value === 2 ? 0 : 2);
                }}
              >
                <path d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
              </svg>
              <svg
                style={{
                  color:
                    value >= 3
                      ? '#FDC432'
                      : hoveredSvg >= 3
                        ? '#FDC432'
                        : '#cbd5e0',
                  opacity:
                    (value >= 3 && hoveredSvg >= 3) || value < 3 ? 0.5 : 1,
                }}
                className="ms-1 size-[40px] opacity-50 transition-colors delay-0 duration-200 ease-in-out"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                viewBox="0 0 22 20"
                onMouseEnter={() => setHoveredSvg(3)}
                onMouseLeave={() => setHoveredSvg(0)}
                onClick={() => {
                  setValue(value === 3 ? 0 : 3);
                }}
              >
                <path d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
              </svg>
              <svg
                style={{
                  color:
                    value >= 4
                      ? '#99D815'
                      : hoveredSvg >= 4
                        ? '#99D815'
                        : '#cbd5e0',
                  opacity:
                    (value >= 4 && hoveredSvg >= 4) || value < 4 ? 0.5 : 1,
                }}
                className="ms-1 size-[40px] opacity-50 transition-colors delay-0 duration-200 ease-in-out"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                viewBox="0 0 22 20"
                onMouseEnter={() => setHoveredSvg(4)}
                onMouseLeave={() => setHoveredSvg(0)}
                onClick={() => {
                  console.log('clicked');
                  setValue(value === 4 ? 0 : 4);
                }}
              >
                <path d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
              </svg>
              <svg
                style={{
                  color:
                    value >= 5
                      ? '#5BB449'
                      : hoveredSvg >= 5
                        ? '#5BB449'
                        : '#cbd5e0',
                  opacity:
                    (value >= 5 && hoveredSvg >= 5) || value < 5 ? 0.5 : 1,
                }}
                className="ms-1 size-[40px] opacity-50 transition-colors delay-0 duration-200 ease-in-out"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                viewBox="0 0 22 20"
                onMouseEnter={() => setHoveredSvg(5)}
                onMouseLeave={() => setHoveredSvg(0)}
                onClick={() => {
                  setValue(value === 5 ? 0 : 5);
                }}
              >
                <path d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
              </svg>
            </div>
            <div className="flex justify-center font-boraml">
              <div className="w-1/2 text-left">1 - 很差</div>
              <div className="w-[220px]" />
              <div className="w-1/2 text-right">5 - 很好</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export { RateBox };
