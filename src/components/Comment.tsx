'use client';

import 'react-toastify/dist/ReactToastify.css';

import Image from 'next/image';
import React, { useState } from 'react';

type CommentProps = {
  school_id?: number;
  professor_id?: number;
  comment: Comments;
};

type Comments = {
  comment_id: number;
  title: string;
  rating: string;
  difficulty: string;
  credit: number;
  attendance: string;
  textbook: string;
  grade: string;
  reselect: string;
  review: string;
  date: string;
  thumbUp: number;
  thumbDown: number;
};

const Comment = ({ comment }: { comment: CommentProps }) => {
  const [thumbup, setThumbup] = useState(null);
  const [thumbdown, setThumbdown] = useState(null);

  const [isLikeClicked, setIsLikeClicked] = useState(false);
  const [isDislikeClicked, setIsDislikeClicked] = useState(false);
  const [isLikeClickable, setIsLikeClickedable] = useState(true);
  const [isDislikeClickable, setIsDislikeClickedable] = useState(true);

  return (
    <div className="py-[16px] text-green-400">
      <div className="flex bg-green-50 px-[24px] pt-[21px]">
        <div className="flex flex-row">
          <div className="flex flex-col">
            <div className="mb-[24px] box-border align-baseline">
              <div className="box-border flex flex-col align-baseline">
                <div className="mb-[4px] box-border self-center align-baseline text-[14px] font-semibold">
                  评分
                </div>
                <div className="mb-[8px] box-border size-[64px] self-center bg-yellow-400 px-[6px] py-[14px] align-baseline text-[32px] font-black leading-9">
                  {comment.comment.rating}
                </div>
              </div>
            </div>

            <div className="mb-[24px] box-border align-baseline">
              <div className="box-border flex flex-col align-baseline">
                <div className="mb-[4px] box-border self-center align-baseline text-[14px] font-semibold">
                  难度
                </div>
                <div className="mb-[8px] box-border size-[64px] self-center bg-yellow-400 px-[6px] py-[14px] align-baseline text-[32px] font-black leading-9">
                  {comment.comment.difficulty}
                </div>
              </div>
            </div>
          </div>

          <div className="ml-[48px] flex w-full flex-col text-left">
            <div className="mb-[10px] mt-[5px] flex min-h-[31px] items-center justify-between">
              <div className="flex items-center self-center pr-[22px] text-[16px] font-black ">
                {comment.comment.title}
              </div>
              <div className="text-[14px] font-bold">
                {comment.comment.date}
              </div>
            </div>

            <div className="mb-[14px] flex flex-wrap">
              <div className="mb-[8px] mr-[16px] whitespace-nowrap py-[4px] text-[16px] leading-6">
                学分:
                <span className="font-bold">{comment.comment.credit}</span>
              </div>
              <div className="mb-[8px] mr-[16px] whitespace-nowrap py-[4px] text-[16px] leading-6">
                出勤:
                <span className="font-bold">{comment.comment.attendance}</span>
              </div>
              <div className="mb-[8px] mr-[16px] whitespace-nowrap py-[4px] text-[16px] leading-6">
                教材:
                <span className="font-bold">{comment.comment.textbook}</span>
              </div>
              <div className="mb-[8px] mr-[16px] whitespace-nowrap py-[4px] text-[16px] leading-6">
                成绩:
                <span className="font-bold">{comment.comment.grade}</span>
              </div>
              <div className="mb-[8px] mr-[16px] whitespace-nowrap py-[4px] text-[16px] leading-6">
                再选:
                <span className="font-bold">{comment.comment.reselect}</span>
              </div>
            </div>

            <div className="mb-[30px] flex-auto shrink grow text-[16px] leading-6">
              {comment.comment.review}
            </div>

            <div className="mb-[32px] mt-[24px] flex justify-between">
              <div className="flex items-center">
                <div className="mr-[16px] flex flex-row items-center font-bold">
                  <Image
                    width={20}
                    height={20}
                    src={
                      isLikeClicked
                        ? '/assets/images/thumbup_fill.svg'
                        : '/assets/images/thumbup.svg'
                    }
                    alt="like"
                    className="mr-[5px] h-[16px]"
                    onClick={() => {
                      if (!isLikeClicked && isLikeClickable) {
                        let url;
                        if (comment.school_id) {
                          url = `/api/school/${comment.school_id}/comment/${comment.comment.comment_id}/like`;
                        } else if (comment.professor_id) {
                          url = `/api/professor/${comment.professor_id}/comment/${comment.comment.comment_id}/like`;
                        }

                        if (url) {
                          fetch(url, {
                            method: 'POST',
                          })
                            .then((response) => response.json())
                            .then((data) => {
                              setThumbup(data.thumbUp);
                              setIsLikeClicked(true);
                              setIsDislikeClickedable(false);
                            })
                            .catch((error) => console.error('Error:', error));
                        }
                      }
                    }}
                  />
                  <div className="mt-[5px] text-[12px]">
                    {thumbup || comment.comment.thumbUp}
                  </div>
                </div>

                <div className="mr-[16px] flex flex-row items-center font-bold">
                  <Image
                    width={20}
                    height={20}
                    src={
                      isDislikeClicked
                        ? '/assets/images/thumbdown_fill.svg'
                        : '/assets/images/thumbdown.svg'
                    }
                    alt="like"
                    className="mr-[5px] h-[16px]"
                    onClick={() => {
                      if (!isDislikeClicked && isDislikeClickable) {
                        let url;
                        if (comment.school_id) {
                          url = `/api/school/${comment.school_id}/comment/${comment.comment.comment_id}/dislike`;
                        } else if (comment.professor_id) {
                          url = `/api/professor/${comment.professor_id}/comment/${comment.comment.comment_id}/dislike`;
                        }

                        if (url) {
                          fetch(url, {
                            method: 'POST',
                          })
                            .then((response) => response.json())
                            .then((data) => {
                              setThumbdown(data.thumbDown);
                              setIsDislikeClicked(true);
                              setIsLikeClickedable(false);
                            })
                            .catch((error) => console.error('Error:', error));
                        }
                      }
                    }}
                  />
                  <div className="mt-[5px] text-[12px]">
                    {thumbdown || comment.comment.thumbDown}
                  </div>
                </div>
              </div>

              <div className="flex items-center">
                <button
                  type="button"
                  aria-label="Like"
                  className="cursor-pointer"
                >
                  <div className="flex items-center">
                    <svg
                      width="26"
                      height="20"
                      viewBox="0 0 26 30"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M21.4522 6.71525L17.8516 2.99833L14.2509 6.71525L13.2998 5.73342L17.8516 1.03467L22.4033 5.73342L21.4522 6.71525Z"
                        fill="black"
                        stroke="black"
                        strokeWidth="0.5"
                      />
                      <path
                        d="M17.1792 2.01489H18.5379V16.7423H17.1792V2.01489Z"
                        fill="black"
                        stroke="black"
                        strokeWidth="0.5"
                      />
                      <path
                        d="M24.6454 25.1614H11.0581C9.90319 25.1614 9.02002 24.2497 9.02002 23.0575V10.434C9.02002 9.24178 9.90319 8.33008 11.0581 8.33008H15.8137V9.73269H11.0581C10.6505 9.73269 10.3787 10.0132 10.3787 10.434V23.0575C10.3787 23.4783 10.6505 23.7588 11.0581 23.7588H24.6454C25.053 23.7588 25.3248 23.4783 25.3248 23.0575V10.434C25.3248 10.0132 25.053 9.73269 24.6454 9.73269H19.8899V8.33008H24.6454C25.8003 8.33008 26.6835 9.24178 26.6835 10.434V23.0575C26.6835 24.2497 25.8003 25.1614 24.6454 25.1614Z"
                        fill="black"
                        stroke="black"
                        strokeWidth="0.5"
                      />
                    </svg>
                  </div>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export { Comment };
