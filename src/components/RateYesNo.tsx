'use client';

import Image from 'next/image';
import React from 'react';

const RateYesNo = ({
  imageUrl,
  selection,
  setSelection,
  title,
  textYes,
  textNo,
}: {
  imageUrl: string;
  selection: string;
  setSelection: React.Dispatch<React.SetStateAction<string>>;
  title: string;
  textYes: string;
  textNo: string;
}) => {
  const handleSelection = (option: string) => {
    setSelection(option);
  };

  return (
    <div className="mb-[20px] min-w-[300px] text-left">
      <div className="divide-solid rounded-[40px] border-[1px] border-green-800 bg-green-50 px-[28px] pb-[20px] pt-[16px] text-left shadow-lg">
        <div className="flex flex-col text-left">
          <div className="mb-[10px] flex min-w-[235px] flex-row items-center text-[16px] font-bold">
            <Image src={imageUrl} alt="rating1" width={50} height={50} />
            <span className="ml-2">{title}</span>
          </div>

          <div className="flex flex-col items-center">
            <div className="flex max-w-[200px] flex-row items-center justify-between space-x-4 text-[14px]">
              <div className="flex flex-row items-center space-x-2 text-green-400">
                <button
                  type="button"
                  style={{
                    backgroundColor: selection === 'Yes' ? '#74BA87' : 'white',
                    borderColor: selection === 'Yes' ? '#74BA87' : '#edf2f7',
                  }}
                  className="relative rounded-full border-[2px] border-gray-200 bg-white p-4 text-white"
                  onClick={() => handleSelection('Yes')}
                >
                  {selection === 'Yes' && (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      className="absolute left-1/2 top-1/2 size-6 -translate-x-1/2 -translate-y-1/2"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M5 13l4 4L19 7"
                      />
                    </svg>
                  )}
                </button>
                <span>{textYes}</span>
              </div>
              <div className="flex flex-row items-center space-x-2 text-green-400">
                <button
                  type="button"
                  style={{
                    backgroundColor: selection === 'No' ? '#F87125' : 'white',
                    borderColor: selection === 'No' ? '#F87125' : '#edf2f7',
                  }}
                  className="relative rounded-full border-[2px] border-gray-200 bg-white p-4 text-white"
                  onClick={() => handleSelection('No')}
                >
                  {selection === 'No' && (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      className="absolute left-1/2 top-1/2 size-6 -translate-x-1/2 -translate-y-1/2"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M6 18L18 6M6 6l12 12"
                      />
                    </svg>
                  )}
                </button>
                <span>{textNo}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export { RateYesNo };
