'use client';

import { useRouter } from 'next/navigation';
import React, { useState } from 'react';

import { DropDownBox } from '@/components/DropDownBox';
import { RateBox } from '@/components/RateBox';
import { RateYesNo } from '@/components/RateYesNo';

import { RateCommentForm } from './RateCommentForm';

interface RateProfessorFormProps {
  id: number;
  courses: string[];
}

const RateProfessorForm = ({ info }: { info: RateProfessorFormProps }) => {
  const router = useRouter();
  const [professor, setProfessor] = useState(0);
  const [difficulty, setDifficulty] = useState(0);
  const [course, setCourse] = useState('');

  const [comment, setComment] = useState('');
  const [courseOnline, setCourseOnline] = useState('No');
  const [takeAgain, setTakeAgain] = useState('');
  const [takeCredit, setTakeCredit] = useState('');
  const [needBook, setNeedBook] = useState('');
  const [attendance, setAttendance] = useState('');
  const [grade, setGrade] = useState('');
  const grades = [
    'Not Sure',
    'A+',
    'A',
    'A-',
    'B+',
    'B',
    'B-',
    'C+',
    'C',
    'C-',
    'D+',
    'D',
    'D-',
    'F',
  ];

  const images = [
    '/assets/images/r7.png',
    '/assets/images/r8.png',
    '/assets/images/r9.png',
    '/assets/images/r10.png',
    '/assets/images/r11.png',
    '/assets/images/r12.png',
    '/assets/images/r13.png',
    '/assets/images/r14.png',
  ];

  return (
    <div className="mt-[20px]">
      <DropDownBox
        value={course}
        setValue={setCourse}
        imageUrl={images[2] || ''}
        options={info.courses}
        selection={courseOnline}
        setSelection={setCourseOnline}
        title="搜索你的课号"
        optionTitle="选课号"
      />

      <RateBox
        value={professor}
        setValue={setProfessor}
        imageUrl={images[0] || ''}
        title="教授"
      />
      <RateBox
        value={difficulty}
        setValue={setDifficulty}
        imageUrl={images[1] || ''}
        title="难度"
      />

      <RateYesNo
        imageUrl={images[3] || ''}
        selection={takeAgain}
        setSelection={setTakeAgain}
        title="会再上他的课吗？"
        textYes="会的"
        textNo="不会"
      />

      <RateYesNo
        imageUrl={images[4] || ''}
        selection={takeCredit}
        setSelection={setTakeCredit}
        title="这门课你拿到学分了吗？"
        textYes="拿了"
        textNo="没有"
      />

      <RateYesNo
        imageUrl={images[5] || ''}
        selection={needBook}
        setSelection={setNeedBook}
        title="这位教授要求教材吗？"
        textYes="要买"
        textNo="不用"
      />

      <RateYesNo
        imageUrl={images[6] || ''}
        selection={attendance}
        setSelection={setAttendance}
        title="是一定要出勤吗？"
        textYes="是的"
        textNo="不是"
      />

      <DropDownBox
        value={grade}
        setValue={setGrade}
        imageUrl={images[7] || ''}
        options={grades}
        title="选择你收到的成绩"
        optionTitle="选择成绩"
      />

      <div className="mb-[20px]">
        <RateCommentForm
          value={comment}
          setValue={setComment}
          info="讨论教授的专业能力，包括教学风格和清晰传达材料的能力"
          placeholder="关于这位教授，你跟其他学生说一些什么？"
        />
      </div>

      <div className="divide-solid rounded-[40px] border-[1px] border-green-800 bg-green-50 px-[28px] pb-[20px] pt-[16px] text-left text-[13px] text-green-400 shadow-lg">
        <div className="text-center">
          <div className="flex flex-col justify-center ">
            <div className="mb-[16px]">
              单击“提交”按钮，即表示我确认我已阅读并同意快评教授评分网站
              <span className="font-bold">指南</span>、
              <span className="font-bold">使用条款</span>和
              <span className="font-bold">隐私政策</span>。
              提交的数据成为快评教授评分的财产。
            </div>
            <div className="flex justify-center">
              <div className="relative text-left">
                <button
                  disabled={
                    professor === 0 ||
                    difficulty === 0 ||
                    course === '' ||
                    takeAgain === '' ||
                    takeCredit === '' ||
                    needBook === '' ||
                    attendance === '' ||
                    grade === ''
                  }
                  style={{
                    cursor:
                      professor === 0 ||
                      difficulty === 0 ||
                      course === '' ||
                      takeAgain === '' ||
                      takeCredit === '' ||
                      needBook === '' ||
                      attendance === '' ||
                      grade === ''
                        ? 'not-allowed'
                        : 'pointer',
                    opacity:
                      professor === 0 ||
                      difficulty === 0 ||
                      course === '' ||
                      takeAgain === '' ||
                      takeCredit === '' ||
                      needBook === '' ||
                      attendance === '' ||
                      grade === ''
                        ? 0.5
                        : 1,
                  }}
                  className="min-w-[150px] rounded-3xl bg-yellow-400 px-[22px] py-1 text-[14px] font-bold text-green-400 shadow-md hover:bg-yellow-500"
                  type="button"
                  onClick={async () => {
                    const response = await fetch(
                      `/api/add/rate-professor/${info.id}`,
                      {
                        method: 'POST',
                        headers: {
                          'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                          course,
                          courseOnline,
                          professor,
                          difficulty,
                          takeAgain,
                          takeCredit,
                          needBook,
                          attendance,
                          grade,
                          comment,
                        }),
                      },
                    );

                    if (!response.ok) {
                      // Handle error
                      console.error('Error posting comment');
                    } else {
                      // Handle success
                      router.push(`/professor/${info.id}`);
                    }
                  }}
                >
                  提交
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export { RateProfessorForm };
