'use client';

import { useEffect } from 'react';

const Test = () => {
  useEffect(() => {
    console.log('Test component has rendered/re-rendered');
  }, []);

  return (
    <div>
      <h1>Test</h1>
    </div>
  );
};

export { Test };
