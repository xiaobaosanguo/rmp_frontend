'use client';

import 'chart.js/auto';

import ChartDataLabels from 'chartjs-plugin-datalabels';
import React from 'react';
import { Bar } from 'react-chartjs-2';

interface RateChartProps {
  dataArray: number[];
}

const options = {
  indexAxis: 'y',
  borderSkipped: false,
  plugins: {
    tooltip: {
      callbacks: {
        label(context: { raw: any }) {
          return context.raw;
        },
        title() {
          return '';
        },
      },
      position: 'nearest',
      displayColors: false,
    },
    legend: {
      display: false,
    },
    datalabels: {
      align: 'end',
      anchor: 'end',
      color: '#0A6724',
      offset(context: any) {
        const value = context.dataset.data[context.dataIndex];
        return value === 0 ? 0 : -20;
      },
      font: {
        weight: '500',
        size: 14,
      },
      formatter(value: number) {
        return `${value}`;
      },
    },
  },
  scales: {
    x: {
      display: false,
      grid: {
        display: false,
        drawBorder: false,
        drawTicks: false,
      },
      ticks: {
        display: false,
      },
    },
    y: {
      display: true,
      grid: {
        display: false,
        drawBorder: false,
        drawTicks: false,
      },
      ticks: {
        color: '#0A6724',
        font: {
          weight: '500',
          size: 14,
        },
        padding: 10,
      },
    },
  },
};

const RateChart: React.FC<RateChartProps> = ({ dataArray }) => {
  const data = {
    labels: ['非常好 5', '还不错 4', '正常 3', '一般 2', '不太好 1'],
    datasets: [
      {
        data: dataArray,
        backgroundColor: 'rgba(255, 173, 0, 1)',
        borderColor: 'rgba(255, 173, 0, 1)',
        borderWidth: 1,
        barPercentage: 0.9,
        catefgoryPercentage: 0.8,
      },
    ],
  };

  const horizontalBackgroundPlugin = {
    id: 'horizontalBackgroundPlugin',
    beforeDatasetDraw(chart: any) {
      const {
        ctx,
        chartArea: { left, width, height },
        scales: { y },
      } = chart;
      const barPercentage =
        (data.datasets[0] as { barPercentage?: number }).barPercentage ?? 0.9; // Fix: Added type assertion and nullish coalescing operator
      const categoryPercentage =
        (data.datasets[0] as { catefgoryPercentage?: number })
          .catefgoryPercentage ?? 0.8; // Fix: Added type assertion and nullish coalescing operator
      const barThickness =
        (height / data.labels.length) * barPercentage * categoryPercentage;
      ctx.save();
      ctx.fillStyle = 'rgba(211, 211, 211, 0.5)';
      data.labels.forEach((label: string, index: number) => {
        const yValue = y.getPixelForValue(index);
        ctx.fillRect(left, yValue - barThickness / 2, width, barThickness);
      });
      ctx.restore();
    },
  };

  return (
    <Bar
      data={data}
      plugins={[ChartDataLabels, horizontalBackgroundPlugin]}
      options={options}
    />
  );
};

export { RateChart };
