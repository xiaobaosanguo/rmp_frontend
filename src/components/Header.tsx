'use client';

import Image from 'next/image';

import { SearchProfessor } from './SearchProfessor';

const Header = () => {
  return (
    <div className="sticky top-0 z-10 w-full bg-green-200">
      <div className="flex flex-col">
        <header className="mx-auto my-0 flex w-full items-center px-[24px] py-[12px]">
          <div className="flex w-full flex-row items-center justify-between">
            <div className="drop-shadow-logo bg-gradient-to-b from-white to-green-900 bg-clip-text text-center text-[30px] font-normal text-transparent">
              <div className="flex w-auto items-center justify-between ">
                qp
              </div>
            </div>
            <div className="w-full min-w-[300px] font-bold text-white">
              <div className="mx-auto my-0 flex w-full flex-row items-center justify-center space-x-3">
                <div className="flex flex-row items-center justify-between space-x-2 text-[14px]">
                  <div>搜教授名</div>
                  <Image
                    src="/assets/images/search.svg"
                    alt="icon description"
                    width={24}
                    height={24}
                  />
                </div>
                <SearchProfessor />
              </div>
            </div>
            <div />
          </div>
        </header>
      </div>
    </div>
  );
};

export { Header };
