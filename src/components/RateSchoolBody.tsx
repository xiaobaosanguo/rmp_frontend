'use client';

import { useParams } from 'next/navigation';
import React, { useEffect, useState } from 'react';

import { RateSchoolForm } from '@/components/RateSchoolForm';
import type { School } from '@/models/school';

const RateSchoolBody = () => {
  const searchParams = useParams();
  const id = searchParams?.id;
  const [schoolData, setSchoolData] = useState<School>();

  useEffect(() => {
    if (id) {
      fetch(`/api/school/${id}`)
        .then((response) => response.json())
        .then((data) => setSchoolData(data.data))
        .catch((error) => console.error('Error:', error));
    }
  }, [id]);

  return (
    schoolData && (
      <div className="p-[20px] md:p-[40px]">
        <header className="sticky h-auto w-full">
          <div className="flex justify-between">
            <div className="flex flex-col">
              <div className="text-[18px] font-medium text-green-500">
                {schoolData.schoolInfo.city}, {schoolData.schoolInfo.province}
                {}
              </div>
              <div className="flex flex-col items-baseline justify-between sm:flex-row">
                <div className="flex flex-col items-baseline md:flex-row">
                  <span className="mr-[10px] text-[35px] font-bold text-green-400">
                    评价:
                  </span>
                  <div className="felx-row flex items-baseline">
                    <div className="mr-[10px] text-[35px] font-bold text-green-400">
                      {schoolData.schoolInfo.cn_name}{' '}
                      <span className="text-[20px] font-medium text-green-500">
                        UCLA
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <RateSchoolForm id={schoolData.id} />
      </div>
    )
  );
};

export { RateSchoolBody };
