'use client';

import { useRouter } from 'next/navigation';
import React from 'react';

type SchoolInfo = {
  schoolId: number;
  city: string;
  province: string;
  schoolName: string;
  abbreviation: string;
};

const SubmitBtn = ({ schoolInfo }: { schoolInfo: SchoolInfo }) => {
  const router = useRouter();

  return (
    <button
      type="button"
      className="min-w-[110px] mb-2 rounded-3xl bg-yellow-400 px-[22px] py-1 text-[15px] font-bold text-white shadow-md hover:bg-yellow-500"
      onClick={() => {
        console.log('评价学校');
        router.push(`/add/rate-school/${schoolInfo.schoolId}`);
      }}
    >
      评价学校
    </button>
  );
};

export { SubmitBtn };
