'use client';

import Image from 'next/image';
import { useParams } from 'next/navigation';
import { useEffect, useState } from 'react';

import { Comment } from '@/components/Comment';
import type { School } from '@/models/school';

import { SchoolRatingGrid } from './SchoolRatingGrid';
import { SubmitBtn } from './SubmitBtn';

const SchoolBody = () => {
  const searchParams = useParams();
  const id = searchParams?.id;
  const [schoolData, setSchoolData] = useState<School>();

  useEffect(() => {
    if (id) {
      fetch(`/api/school/${id}`)
        .then((response) => response.json())
        .then((data) => setSchoolData(data.data))
        .catch((error) => console.error('Error:', error));
    }
  }, [id]);

  return (
    schoolData && (
      <div className="p-[20px] md:p-[40px]">
        <div className="text-[18px] font-medium text-green-500">
          {schoolData.schoolInfo.city}, {schoolData.schoolInfo.province}, {}
        </div>
        <div className="flex flex-wrap items-baseline justify-between">
          <div className="flex flex-row items-baseline">
            <div className="mr-[10px] text-[35px] font-bold text-green-400">
              {schoolData.schoolInfo.cn_name}
            </div>
            <div className="font-medium text-green-500">
              {schoolData.schoolInfo.abbr_name}
            </div>
          </div>
          <SubmitBtn
            schoolInfo={{
              schoolId: schoolData.id,
              city: schoolData.schoolInfo.city,
              province: schoolData.schoolInfo.province,
              schoolName: schoolData.schoolInfo.cn_name,
              abbreviation: schoolData.schoolInfo.abbr_name,
            }}
          />
        </div>
        <div className="mb-[16px] flex flex-row items-center space-x-1">
          <div className="text-[12px] text-green-400 underline underline-offset-2">
            查看所有教授
          </div>
          <Image
            src="/assets/images/allProf.svg"
            alt="see all profs"
            width={20}
            height={20}
          />
        </div>

        {/* <SubmitBtn schoolInfo={schoolInfo} /> */}

        <div className="mb-10 flex flex-col items-center justify-between md:mb-0 md:flex-row">
          <div className="mb-4 flex flex-col items-center text-[50px] font-semibold text-green-400 md:mb-0">
            {schoolData.schoolInfo.rating.toFixed(1)}
            <span className="text-[15px] font-medium text-yellow-600">
              总评分
            </span>
          </div>
          <div className="flex justify-center md:pr-[24px]">
            <SchoolRatingGrid
              reputation_rating={4}
              food_rating={4}
              facilities_rating={4}
              location_rating={4}
              safety_rating={4}
              social_rating={4}
            />
          </div>
        </div>

        <div className="flex flex-row text-[16px] font-semibold text-green-400">
          {schoolData.schoolInfo.commentsTotal}
          <span>条评论</span>
        </div>

        {schoolData.schoolInfo.comments.map((comment, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <li key={index} className="list-item list-none">
            <Comment
              comment={{
                school_id: schoolData.id,
                comment: {
                  comment_id: comment.comment_id,
                  title: comment.course_title,
                  rating: comment.rating,
                  difficulty: comment.difficulty,
                  credit: comment.credit,
                  attendance: comment.attendance,
                  textbook: comment.textbook,
                  grade: comment.grade,
                  reselect: comment.reselect,
                  review: comment.review,
                  date: comment.date,
                  thumbUp: comment.thumbUp,
                  thumbDown: comment.thumbDown,
                },
              }}
            />
          </li>
        ))}
      </div>
    )
  );
};

export { SchoolBody };
