'use client';

import { useRouter } from 'next/navigation';
import React, { useEffect, useState } from 'react';

type Result = {
  schoolId: number;
  name: string;
  cn_name: string;
  city: string;
  province: string;
  country: string;
};

const Search = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [isFocused, setIsFocused] = useState(false);

  const router = useRouter();

  const [results, setResults] = useState<Result[]>([]);

  const handleSearchChange = (event: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setSearchTerm(event.target.value);
  };

  useEffect(() => {
    if (searchTerm !== '') {
      fetch(`/api/search/school?q=${encodeURIComponent(searchTerm)}`)
        .then((response) => response.json())
        .then((data) => setResults(data))
        .catch((error) => console.error('Error:', error));
    } else {
      setResults([]);
    }
  }, [searchTerm]);

  return (
    <div className="max-h-[70px] mx-auto w-full max-w-sm">
      <input
        className={`w-full rounded-3xl bg-white px-4 py-2 text-xl text-gray-700 ring-1 ring-black/5 focus:border-indigo-500 focus:outline-none  ${results.length > 0 && isFocused ? 'rounded-b-none' : ''} ${results.length > 0 && isFocused ? 'border-b-0' : ''}`}
        type="text"
        placeholder=""
        value={searchTerm}
        onChange={handleSearchChange}
        onFocus={() => setIsFocused(true)}
        onBlur={() => {
          setTimeout(() => {
            setIsFocused(false);
          }, 100);
        }}
      />

      {/* Display search results */}

      {results.length > 0 && (
        <div
          className="overflow-auto max-h-40 relative max-w-sm mx-auto bg-white dark:bg-slate-800 dark:highlight-white/5 shadow-lg ring-1 ring-black/5 rounded-b-3xl flex flex-col divide-y dark:divide-slate-200/5"
          style={{ display: isFocused ? 'block' : 'none' }}
        >
          {results.map((result) => (
            <div
              key={result.schoolId}
              role="presentation"
              className="hover:bg-green-1200 group flex flex-col bg-white px-4 py-2 text-gray-700 transition-colors duration-200 hover:text-white"
              onClick={() => {
                router.push(`/school/${result.schoolId}`);
              }}
              onKeyDown={() => {
                router.push(`/school/${result.schoolId}`);
              }}
            >
              <span className="text-green-1100 text-xl font-medium group-hover:text-white">
                {result.name}
              </span>
              <span className="text-green-1000 text-sm font-medium">
                {result.city}, {result.province}
              </span>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export { Search };
