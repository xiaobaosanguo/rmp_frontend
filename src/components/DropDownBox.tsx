'use client';

import Image from 'next/image';
import React, { useState } from 'react';

import { Dropdown } from '@/components/DropDown';

const DropDownBox = ({
  value,
  setValue,
  imageUrl,
  options,
  selection,
  setSelection,
  title,
  optionTitle,
}: {
  value: string;
  setValue: React.Dispatch<React.SetStateAction<string>>;
  imageUrl: string;
  options: string[];
  selection?: string;
  setSelection?: React.Dispatch<React.SetStateAction<string>>;
  title: string;
  optionTitle?: string;
}) => {
  const [isOpen, setIsOpen] = useState(false);

  const handleSelection = (option: string) => {
    if (setSelection) {
      setSelection((prevSelection) => (prevSelection === option ? '' : option));
    }
  };

  return (
    <div className="mb-[20px] min-w-[300px] text-left">
      <div className="divide-solid rounded-[40px] border-[1px] border-green-800 bg-green-50 px-[28px] pb-[20px] pt-[16px] text-left shadow-lg">
        <div className="flex flex-col text-left">
          <div className="mb-[10px] flex flex-row items-center text-[16px] font-bold">
            <Image src={imageUrl} alt="rating1" width={50} height={50} />
            <span className="ml-2">{title}</span>
          </div>

          <div className="flex flex-col items-center space-x-4 text-[14px]">
            <Dropdown
              isOpen={isOpen}
              setIsOpen={setIsOpen}
              selectedOption={value}
              setSelectedOption={setValue}
              options={options}
              optionTitle={optionTitle}
            />
            {setSelection && (
              <div className="mt-[10px] min-w-[200px] flex flex-row items-center space-x-2 text-green-400">
                <button
                  type="button" // Add the type attribute to the button element
                  style={{
                    backgroundColor: selection === 'Yes' ? '#74BA87' : 'white',
                    borderColor: selection === 'Yes' ? '#74BA87' : '#edf2f7',
                  }}
                  className="relative rounded-full border-[2px] border-gray-200 bg-white p-4 text-white"
                  onClick={() => handleSelection('Yes')}
                >
                  {selection === 'Yes' && (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      className="absolute left-1/2 top-1/2 size-6 -translate-x-1/2 -translate-y-1/2"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M5 13l4 4L19 7"
                      />
                    </svg>
                  )}
                </button>
                <span>这门课是否是网课</span>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export { DropDownBox };
