'use client';

/* eslint-disable react/no-array-index-key */
/* eslint-disable no-nested-ternary */

import Image from 'next/image';

interface RatingGridProps {
  reputation_rating: number;
  food_rating: number;
  facilities_rating: number;
  location_rating: number;
  safety_rating: number;
  social_rating: number;
}

const SchoolRatingGrid: React.FC<RatingGridProps> = ({
  reputation_rating,
  food_rating,
  facilities_rating,
  location_rating,
  safety_rating,
  social_rating,
}) => {
  return (
    <div className="grid w-full grid-cols-1 gap-4 md:w-auto md:grid-cols-2">
      <div className=" flex h-[34px] items-center space-x-[5px]">
        <Image
          src="/assets/images/sr/1.png"
          alt="attendance"
          width={30}
          height={30}
        />
        <span className="text-[14px] text-green-400">声誉</span>
        <div className="flex flex-row">
          {[...Array(5)].map((_, i) => (
            <Image
              key={i}
              src={
                i < reputation_rating
                  ? '/assets/images/sr/star1.svg'
                  : '/assets/images/sr/star2.svg'
              }
              alt="star"
              width={24}
              height={24}
            />
          ))}
        </div>
        <div className="flex size-9 items-center justify-center rounded-full bg-yellow-400">
          <span className="px-4 font-semibold text-white">
            {reputation_rating.toFixed(1)}
          </span>
        </div>
      </div>
      <div className=" flex h-[34px] items-center space-x-[5px]">
        <Image
          src="/assets/images/sr/2.png"
          alt="attendance"
          width={30}
          height={30}
        />
        <span className="text-[14px] text-green-400">食物</span>
        <div className="flex flex-row">
          {[...Array(5)].map((_, i) => (
            <Image
              key={i}
              src={
                i < food_rating
                  ? '/assets/images/sr/star1.svg'
                  : '/assets/images/sr/star2.svg'
              }
              alt="star"
              width={24}
              height={24}
            />
          ))}
        </div>
        <div className="flex size-9 items-center justify-center rounded-full bg-yellow-400">
          <span className="px-4 font-semibold text-white">
            {food_rating.toFixed(1)}
          </span>
        </div>
      </div>
      <div className=" flex h-[34px] items-center space-x-[5px]">
        <Image
          src="/assets/images/sr/3.png"
          alt="attendance"
          width={30}
          height={30}
        />
        <span className="text-[14px] text-green-400">设施</span>
        <div className="flex flex-row">
          {[...Array(5)].map((_, i) => (
            <Image
              key={i}
              src={
                i < facilities_rating
                  ? '/assets/images/sr/star1.svg'
                  : '/assets/images/sr/star2.svg'
              }
              alt="star"
              width={24}
              height={24}
            />
          ))}
        </div>
        <div className="flex size-9 items-center justify-center rounded-full bg-yellow-400">
          <span className="px-4 font-semibold text-white">
            {facilities_rating.toFixed(1)}
          </span>
        </div>
      </div>
      <div className=" flex h-[34px] items-center space-x-[5px]">
        <Image
          src="/assets/images/sr/4.png"
          alt="attendance"
          width={30}
          height={30}
        />
        <span className="text-[14px] text-green-400">位置</span>
        <div className="flex flex-row">
          {[...Array(5)].map((_, i) => (
            <Image
              key={i}
              src={
                i < location_rating
                  ? '/assets/images/sr/star1.svg'
                  : '/assets/images/sr/star2.svg'
              }
              alt="star"
              width={24}
              height={24}
            />
          ))}
        </div>
        <div className="flex size-9 items-center justify-center rounded-full bg-yellow-400">
          <span className="px-4 font-semibold text-white">
            {location_rating.toFixed(1)}
          </span>
        </div>
      </div>
      <div className=" flex h-[34px] items-center space-x-[5px]">
        <Image
          src="/assets/images/sr/5.png"
          alt="attendance"
          width={30}
          height={30}
        />
        <span className="text-[14px] text-green-400">安全</span>
        <div className="flex flex-row">
          {[...Array(5)].map((_, i) => (
            <Image
              key={i}
              src={
                i < safety_rating
                  ? '/assets/images/sr/star1.svg'
                  : '/assets/images/sr/star2.svg'
              }
              alt="star"
              width={24}
              height={24}
            />
          ))}
        </div>
        <div className="flex size-9 items-center justify-center rounded-full bg-yellow-400">
          <span className="px-4 font-semibold text-white">
            {safety_rating.toFixed(1)}
          </span>
        </div>
      </div>
      <div className=" flex h-[34px] items-center space-x-[5px]">
        <Image
          src="/assets/images/sr/6.png"
          alt="attendance"
          width={30}
          height={30}
        />
        <span className="text-[14px] text-green-400">社交</span>
        <div className="flex flex-row">
          {[...Array(5)].map((_, i) => (
            <Image
              key={i}
              src={
                i < social_rating
                  ? '/assets/images/sr/star1.svg'
                  : '/assets/images/sr/star2.svg'
              }
              alt="star"
              width={24}
              height={24}
            />
          ))}
        </div>
        <div className="flex size-9 items-center justify-center rounded-full bg-yellow-400">
          <span className="px-4 font-semibold text-white">
            {social_rating.toFixed(1)}
          </span>
        </div>
      </div>
    </div>
  );
};

export { SchoolRatingGrid };
