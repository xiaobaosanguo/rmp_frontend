"use client";

import React, { useState, useEffect, useRef } from "react";
import { useRouter } from "next/navigation";

const SearchComponent = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [results, setResults] = useState<Results>({
    products: [],
    total: 0,
    skip: 0,
    limit: 0,
  });

  const [isFocused, setIsFocused] = useState(false);


  type Result = {
    id: number;
    title: string;
    description: string;
    price: number;
    discountPercentage: number;
    rating: number;
    stock: number;
    brand: string;
    category: string;
    thumbnail: string;
    images: string[];
  };

  type Results = {
    products: Result[];
    total: number;
    skip: number;
    limit: number;
  };

  useEffect(() => {
    if (searchTerm !== "") {
      fetch(`/api/search?q=${encodeURIComponent(searchTerm)}`)
        .then((response) => response.json())
        .then((data) => setResults(data))
        .catch((error) => console.error("Error:", error));
    } else {
      setResults({
        products: [],
        total: 0,
        skip: 0,
        limit: 0,
      });
    }
  }, [searchTerm]);

  const handleResultClick = (result: any) => {
    console.log(`You clicked on ${result.title}`);
  };

  return (
    <div className="relative w-full max-h-70 max-w-sm mx-auto">
      <input
        type="text"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
        onFocus={() => setIsFocused(true)}
          onBlur={() => {
              setIsFocused(false);
          }}
        className={`w-full h-9 px-4 py-2 border border-gray-300 rounded-3xl text-gray-700 bg-white ring-1 ring-black/5 focus:outline-none ${results.products.length > 0 && isFocused ? "rounded-b-none" : ""} ${results.products.length > 0 && isFocused? "border-b-0" : ""}`}
      />
      {results.products.length > 0 && isFocused && (
        <div className="absolute w-full bg-white border-b  border-x border-gray-300 rounded-b-3xl shadow-lg overflow-y-auto max-h-60">
          {results.products.map((result) => (
            <button
              key={result.id}
              className="w-full h-9 px-4 py-2 text-left hover:bg-gray-100 focus:outline-none"
              onClick={() => handleResultClick(result)}
            >
              {result.title}
            </button>
          ))}
        </div>
      )}
    </div>
  );
};

export { SearchComponent };
