'use client';

import React from 'react';

const RateCommentForm = ({
  value,
  setValue,
  info,
  placeholder,
}: {
  value: string;
  setValue: (value: string) => void;
  info: string;
  placeholder?: string;
}) => {
  return (
    <div className="min-w-[300px] text-green-400">
      <div className="divide-solid rounded-[40px] border-[1px] border-green-800 bg-green-50 px-[28px] pb-[20px] pt-[16px] text-left shadow-lg">
        <div className="mb-[16px] text-[16px] font-bold leading-4">写评论</div>
        <div className="mb-[16px] text-[12px] font-bold leading-4">{info}</div>
        <div className="mb-[16px]">
          <div className="mb-[16px] flex flex-col text-[16px] font-bold leading-4">
            <div className="text-[16px] font-bold leading-4">注意事项</div>
          </div>
          <div className="flex flex-col px-[22px] text-left">
            <ol className="mb-[8px] list-disc text-[12px] font-bold">
              <li>如果您使用亵渎或贬义术语，您的评分可能会被删除。</li>
              <li>请参阅评级类别以帮助您更好地阐述您的评论。</li>
              <li>不要忘记校对！</li>
            </ol>
          </div>
        </div>
        <div className="flex flex-col items-end">
          <textarea
            style={{ fontWeight: 'bold' }}
            className="min-h-[200px] w-full resize-none rounded-[10px] border-[1px] border-green-800 bg-green-50 p-[10px] text-[13px] text-green-700 placeholder:text-green-700"
            placeholder={placeholder}
            value={value}
            onChange={(e) => setValue(e.target.value)}
            maxLength={300}
          />
          <span className="text-[13px] font-bold">{value.length} / 300</span>
        </div>
      </div>
    </div>
  );
};

export { RateCommentForm };
