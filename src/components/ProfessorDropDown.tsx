'use client';

interface ProfessorDropdownProps {
  isOpen: boolean;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
  selectedOption: string;
  setSelectedOption: React.Dispatch<React.SetStateAction<string>>;
  options: string[];
  optionTitle?: string;
}

const ProfessorDropDown = ({
  isOpen,
  setIsOpen,
  selectedOption,
  setSelectedOption,
  options,
  optionTitle,
}: ProfessorDropdownProps) => {
  const handleOptionClick = (option: string) => {
    setSelectedOption(option);
    setIsOpen(false);
  };

  return (
    <div className="dropdown relative text-[14px]">
      <button
        className="dropdown-header flex space-x-1 items-center justify-between rounded border-[2px] border-green-1300 bg-white px-4 py-1 text-left text-green-400"
        onClick={() => setIsOpen(!isOpen)}
        type="button"
      >
        <span>{selectedOption || optionTitle}</span>
        {isOpen ? (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="#0A6724"
            viewBox="0 0 24 24"
            stroke="currentColor"
            className="size-6 text-white"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M5 15l7-7 7 7"
            />
          </svg>
        ) : (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="#0A6724"
            viewBox="0 0 24 24"
            stroke="currentColor"
            className="size-6 text-white"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M19 9l-7 7-7-7"
            />
          </svg>
        )}
      </button>
      {isOpen && (
        <div className="dropdown-menu absolute z-10 mt-2 w-full rounded bg-white shadow-lg">
          {options.map((option, index) => (
            // eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions
            <div
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              className="dropdown-option cursor-pointer px-4 py-2 hover:bg-green-200 hover:text-white"
              onClick={() => handleOptionClick(option)}
            >
              {option}
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export { ProfessorDropDown };
