'use client';

import { useParams } from 'next/navigation';
import React, { useEffect, useState } from 'react';

import { RateProfessorForm } from '@/components/RateProfessorForm';
import type { Professor } from '@/models/professor';

const RateProfessorBody = () => {
  const searchParams = useParams();
  const id = searchParams?.id;

  const [professorData, setProfessorData] = useState<Professor>();

  useEffect(() => {
    if (id) {
      fetch(`/api/professor/${id}`)
        .then((response) => response.json())
        .then((data) => setProfessorData(data.data))
        .catch((error) => console.error('Error:', error));
    }
  }, [id]);

  return (
    professorData && (
      <div className="p-[20px] md:p-[40px]">
        <header className="sticky h-auto w-full">
          <div className="flex justify-between">
            <div className="flex flex-col">
              <div className="flex flex-col items-baseline justify-between sm:flex-row">
                <div className="flex flex-col items-baseline md:flex-row">
                  <span className="mr-[10px] text-[35px] font-bold text-green-400">
                    评价:
                  </span>
                  <div className="felx-row flex items-baseline">
                    <div className="mr-[10px] text-[35px] font-bold text-green-400">
                      {professorData.professorInfo.firstName}{' '}
                      {professorData.professorInfo.lastName} 教授
                    </div>
                  </div>
                </div>
              </div>
              <div className="text-[18px] font-medium text-green-500">
                {professorData.professorInfo.department_cn}专业 -{' '}
                {professorData.professorInfo.schoolName_cn} UCLA{}
              </div>
            </div>
          </div>
        </header>
        <RateProfessorForm
          info={{
            id: professorData.id,
            courses: professorData.professorInfo.courses,
          }}
        />
      </div>
    )
  );
};

export { RateProfessorBody };
