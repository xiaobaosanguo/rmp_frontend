'use client';

import { useParams } from 'next/navigation';
import { useEffect, useState } from 'react';

import { Comment } from '@/components/Comment';
import { ProfessorDropDown } from '@/components/ProfessorDropDown';
import type { Professor } from '@/models/professor';

import { RateChart } from './RateChart';

const ProfessorBody = () => {
  const searchParams = useParams();
  const id = searchParams?.id;

  const [professorData, setProfessorData] = useState<Professor>();
  const [selectedCourse, setSelectedCourse] = useState('所有课程');
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    if (id) {
      fetch(`/api/professor/${id}`)
        .then((response) => response.json())
        .then((data) => setProfessorData(data.data))
        .catch((error) => console.error('Error:', error));
    }
  }, [id]);

  return (
    professorData && (
      <div className="p-[20px] md:p-[40px] text-green-400">
        <div className="mb-[24px] flex flex-col items-center justify-center md:flex-row md:justify-between">
          <div className="pb-[25px] pr-[22px]">
            <div>
              <div className="flex justify-between">
                <div className="flex space-x-2">
                  <div className="text-[60px] font-bold">
                    {professorData.professorInfo.rating.toFixed(1)}
                  </div>
                  <div className="relative top-[18px] text-[18px] font-bold">
                    /5
                  </div>
                </div>
              </div>
              <div className="mb-[5px] pt-[5px] text-[15px] font-bold">
                整体质量基于<span className="text-yellow-400">10</span>个评级
              </div>
            </div>
            <div>
              <div className="hyphens-auto text-[40px] font-bold">
                {professorData.professorInfo.firstName}{' '}
                {professorData.professorInfo.lastName}
              </div>
              <div>
                <span className="text-[15px] font-bold text-yellow-400">
                  {professorData.professorInfo.schoolName_cn}
                </span>

                <span className="text-[15px] font-bold">
                  <span className="text-yellow-400">
                    {professorData.professorInfo.department_cn}
                  </span>
                  系教授
                </span>
              </div>
              <div className="flex">
                <div className="flex flex-col pb-[16px] pl-0 pr-[12px] pt-0 text-center">
                  <div className="text-[30px] font-bold">
                    {professorData.professorInfo.reselectPercentage}%
                  </div>
                  <div className="text-[12px] font-normal">会再次选择</div>
                </div>
                <div className="solid flex flex-col border-l-[2px] border-black pb-[16px] pl-[12px] pr-0 pt-0 text-center">
                  <div className="text-[30px] font-bold">
                    {professorData.professorInfo.difficulty.toFixed(1)}
                  </div>
                  <div className="text-[12px] font-normal">难度等级</div>
                </div>
              </div>
              <div className="mt-[8px] flex flex-row justify-start">
                <a
                  className="leading-1 flex w-auto min-w-[110px] justify-center rounded-3xl bg-yellow-400 px-[22px] py-1 text-[15px] font-bold text-white shadow-md hover:bg-yellow-500"
                  href="/add/rate-professor/1"
                >
                  评价教授
                </a>
              </div>
            </div>
          </div>
          <div className="flex w-[500px] flex-col text-[20px] font-semibold">
            评级分布
            <div
              style={{
                minWidth: '324px',
                height: '200px',
              }}
            >
              <RateChart dataArray={[7, 1, 1, 1, 0]} />
            </div>
          </div>
        </div>
        <div className="flex w-full justify-between">
          <div className="border-green-1300 flex w-full items-center justify-center border-b-[1px] text-[16px] font-semibold text-yellow-400">
            <div className="p-2">
              {professorData.professorInfo.commentsTotal}
              <span className="text-green-400">条评论</span>
            </div>
          </div>
          <div className="w-full" />
        </div>

        <div className="flex items-center py-2">
          <ProfessorDropDown
            isOpen={isOpen}
            setIsOpen={setIsOpen}
            selectedOption={selectedCourse}
            setSelectedOption={setSelectedCourse}
            options={['所有课程', ...professorData.professorInfo.courses]}
            optionTitle="所有课程"
          />
        </div>

        {professorData.professorInfo.comments
          .filter(
            (comment) =>
              selectedCourse === '所有课程' ||
              comment.course_title === selectedCourse,
          )
          .map((comment, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <li key={index} className="list-item list-none">
              <Comment
                comment={{
                  professor_id: professorData.id,
                  comment: {
                    comment_id: comment.comment_id,
                    title: comment.course_title,
                    rating: comment.rating.toFixed(1).toString(),
                    difficulty: comment.difficulty.toFixed(1).toString(),
                    credit: comment.credit,
                    attendance: comment.attendance,
                    textbook: comment.textbook,
                    grade: comment.grade,
                    reselect: comment.reselect,
                    review: comment.review,
                    date: comment.date,
                    thumbUp: comment.thumbUp,
                    thumbDown: comment.thumbDown,
                  },
                }}
              />
            </li>
          ))}
      </div>
    )
  );
};

export { ProfessorBody };
