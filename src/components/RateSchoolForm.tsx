'use client';

import { useRouter } from 'next/navigation';
import React, { useState } from 'react';

import { RateBox } from '@/components/RateBox';

import { RateCommentForm } from './RateCommentForm';

const RateSchoolForm = ({ id }: { id: number }) => {
  const router = useRouter();

  const [reputation, setReputation] = useState(0);
  const [facilities, setFacilities] = useState(0);
  const [location, setLocation] = useState(0);
  const [food, setFood] = useState(0);
  const [safety, setSafety] = useState(0);
  const [social, setSocial] = useState(0);

  const [comment, setComment] = useState('');

  const images = [
    '/assets/images/r1.png',
    '/assets/images/r2.png',
    '/assets/images/r3.png',
    '/assets/images/r4.png',
    '/assets/images/r5.png',
    '/assets/images/r6.png',
  ];

  return (
    <div className="">
      <RateBox
        value={reputation}
        setValue={setReputation}
        imageUrl={images[0] || ''}
        title="声誉"
      />
      <RateBox
        value={facilities}
        setValue={setFacilities}
        imageUrl={images[1] || ''}
        title="设施"
      />
      <RateBox
        value={food}
        setValue={setFood}
        imageUrl={images[2] || ''}
        title="食物"
      />
      <RateBox
        value={safety}
        setValue={setSafety}
        imageUrl={images[3] || ''}
        title="安全"
      />
      <RateBox
        value={location}
        setValue={setLocation}
        imageUrl={images[4] || ''}
        title="位置"
      />
      <RateBox
        value={social}
        setValue={setSocial}
        imageUrl={images[5] || ''}
        title="社交"
      />

      <div className="mb-[20px]">
        <RateCommentForm
          value={comment}
          setValue={setComment}
          info="讨论您在这所学校的个人经历。它有什么优点？有什么可以改进的地方？"
          placeholder="关于这所学校，你跟其他学生说一些什么？"
        />
      </div>

      <div className="min-w-[300px] divide-solid rounded-[40px] border-[1px] border-green-800 bg-green-50 px-[28px] pb-[20px] pt-[16px] text-left text-[13px] text-green-400 shadow-lg">
        <div className="text-center">
          <div className="flex flex-col justify-center ">
            <div className="mb-[16px]">
              单击“提交”按钮，即表示我确认我已阅读并同意快评教授评分网站
              <span className="font-bold">指南</span>、
              <span className="font-bold">使用条款</span>和
              <span className="font-bold">隐私政策</span>。
              提交的数据成为快评教授评分的财产。
            </div>
            <div className="flex justify-center">
              <div className="relative text-left">
                <button
                  disabled={
                    reputation === 0 ||
                    facilities === 0 ||
                    location === 0 ||
                    food === 0 ||
                    safety === 0 ||
                    social === 0
                  }
                  style={{
                    cursor:
                      reputation === 0 ||
                      facilities === 0 ||
                      location === 0 ||
                      food === 0 ||
                      safety === 0 ||
                      social === 0
                        ? 'not-allowed'
                        : 'pointer',
                    opacity:
                      reputation === 0 ||
                      facilities === 0 ||
                      location === 0 ||
                      food === 0 ||
                      safety === 0 ||
                      social === 0
                        ? 0.5
                        : 1,
                  }}
                  className="min-w-[150px] rounded-3xl bg-yellow-400 px-[22px] py-1 text-[14px] font-bold text-green-400 shadow-md hover:bg-yellow-500"
                  type="button"
                  onClick={async () => {
                    const response = await fetch(`/api/add/rate-school/${id}`, {
                      method: 'POST',
                      headers: {
                        'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                        reputation,
                        facilities,
                        location,
                        food,
                        safety,
                        social,
                        comment,
                      }),
                    });

                    if (!response.ok) {
                      // Handle error
                      console.error('Error posting comment');
                    } else {
                      // Handle success
                      router.push(`/school/${id}`);
                    }
                  }}
                >
                  提交
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export { RateSchoolForm };
