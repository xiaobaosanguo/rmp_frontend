'use client';

import { useRouter } from 'next/navigation';
import React, { useEffect, useState } from 'react';

type Result = {
  firstname: string;
  lastname: string;
  professorId: string;
  schoolId: number;
  schoolName: string;
  schoolName_cn: string;
};

const SearchProfessor = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [isFocused, setIsFocused] = useState(false);

  const router = useRouter();

  const [results, setResults] = useState<Result[]>([]);

  const handleSearchChange = (event: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setSearchTerm(event.target.value);
  };

  useEffect(() => {
    if (searchTerm !== '') {
      fetch(`/api/search/professor?q=${encodeURIComponent(searchTerm)}`)
        .then((response) => response.json())
        .then((data) => setResults(data))
        .catch((error) => console.error('Error:', error));
    } else {
      setResults([]);
    }
  }, [searchTerm]);

  return (
    <div className="mx-auto max-h-[40px] max-w-[800px]">
      <input
        className={`w-full max-w-[750px] rounded-3xl bg-white px-4 py-2 text-[14px] text-gray-700 ring-1 ring-black/5 focus:border-indigo-500 focus:outline-none  ${results.length > 0 && isFocused ? 'rounded-b-none' : ''} ${results.length > 0 && isFocused ? 'border-b-0' : ''}`}
        type="text"
        placeholder=""
        value={searchTerm}
        onChange={handleSearchChange}
        onFocus={() => setIsFocused(true)}
        onBlur={() => {
          setTimeout(() => {
            setIsFocused(false);
          }, 100);
        }}
      />

      {/* Display search results */}

      {results.length > 0 && (
        <div
          className="dark:highlight-white/5 relative mx-auto flex max-h-40 max-w-sm flex-col divide-y overflow-auto rounded-b-3xl bg-white shadow-lg ring-1 ring-black/5 dark:divide-slate-200/5 dark:bg-slate-800"
          style={{ display: isFocused ? 'block' : 'none' }}
        >
          {results.map((result) => (
            <div
              key={result.schoolId}
              role="presentation"
              className="hover:bg-green-1200 group flex flex-col bg-white px-4 py-2 text-gray-700 transition-colors duration-200 hover:text-white"
              onClick={() => {
                router.push(`/professor/${result.professorId}`);
              }}
              onKeyDown={() => {
                router.push(`/professor/${result.professorId}`);
              }}
            >
              <span className="text-green-1100 text-[15px] font-medium group-hover:text-white">
                {result.firstname} {result.lastname}
              </span>
              <span className="text-green-1000 text-[10px] font-medium">
                {result.schoolName_cn}
              </span>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export { SearchProfessor };
