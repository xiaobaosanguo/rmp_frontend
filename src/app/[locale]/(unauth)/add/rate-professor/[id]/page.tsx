import { getTranslations } from 'next-intl/server';

import { Header } from '@/components/Header';
import { RateProfessorBody } from '@/components/RateProfessorBody';

export async function generateMetadata(props: { params: { locale: string } }) {
  const t = await getTranslations({
    locale: props.params.locale,
    namespace: 'About',
  });

  return {
    title: t('meta_title'),
    description: t('meta_description'),
  };
}

export default function RateProfessor() {
  return (
    <>
      <Header />
      <RateProfessorBody />
    </>
  );
}
