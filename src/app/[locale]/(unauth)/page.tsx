import Image from 'next/image';
import { getTranslations } from 'next-intl/server';
import { ToastContainer } from 'react-toastify';

import { Search } from '@/components/Search';

export async function generateMetadata(props: { params: { locale: string } }) {
  const t = await getTranslations({
    locale: props.params.locale,
    namespace: 'Index',
  });

  return {
    title: t('meta_title'),
    description: t('meta_description'),
  };
}

export default function Index() {
  return (
    <>
      <ToastContainer />
      <div className="sticky top-0 z-10 w-full">
        <div className="flex flex-col">
          <header className="mx-auto my-0 flex w-full items-center px-[24px] py-[12px]">
            <div className="flex w-full flex-row items-center justify-between">
              <div className="drop-shadow-logo bg-gradient-to-b from-green-100 to-green-200 bg-clip-text text-center text-[30px] font-normal text-transparent">
                <div className="flex w-auto items-center justify-between ">
                  qp
                </div>
              </div>
              <div />
            </div>
          </header>
        </div>
      </div>
      {/* a div include image that fullfield this div */}
      <div className="relative h-80 w-full">
        <img
          className="size-full object-cover"
          src="/assets/images/school_bg.png"
        />
        <div className="absolute inset-0 bg-green-200 opacity-85" />
        <div className="absolute inset-x-0 top-10 flex flex-col items-center justify-center">
          <div className="flex flex-row items-baseline">
            <h1 className="text-4xl font-bold text-yellow-300">快评</h1>
            <h1 className="text-2xl text-white">教授评分</h1>
          </div>
          <div className="flex flex-row items-baseline">
            <h1 className="text-sm text-white">输入</h1>
            <h1 className="text-xl font-bold text-white">学校</h1>
            <h1 className="text-sm text-white">名称查询</h1>
          </div>
          <div className="mt-7 w-[80%]">
            <Search />
          </div>

          {/* <div className="mt-2 flex items-baseline">
            <h1 className="text-xs text-white">输入教授全称查询</h1>
          </div> */}
        </div>
      </div>

      <div className="mt-2 flex items-center justify-center">
        <h1 className="text-xl font-bold text-green-300">
          专注海外华人学习体验
        </h1>
      </div>
      <div className="mt-5 flex justify-center p-5">
        <Image
          src="/assets/images/homeblob.png"
          alt="hero"
          width={720}
          height={480}
        />
      </div>
    </>
  );
}
