import { NextResponse } from 'next/server';

// Handles GET requests to /api
export async function GET(request: Request) {
  try {
    const searchQuery = request.url?.split('?')[1]; // Extract the search query from the request URL

    // Check if searchQuery exists
    if (!searchQuery) {
      return NextResponse.json(
        { error: 'Search query is missing' },
        { status: 400 },
      );
    }

    const url = new URL('http://127.0.0.1:5000/search_prof');
    url.search = new URLSearchParams(searchQuery).toString(); // Append the search query to the URL

    const response = await fetch(url.toString()); // Fetch data from the URL

    // Check if the fetch was successful
    if (!response.ok) {
      return NextResponse.json(
        { error: `Fetch failed with status: ${response.status}` },
        { status: response.status },
      );
    }

    const data = await response.json(); // Parse the response to JSON

    console.log(data);

    return NextResponse.json(data); // Return the fetched data
  } catch (error) {
    // Handle any other errors
    if (error instanceof Error) {
      return NextResponse.json(
        { error: `An error occurred: ${error.message}` },
        { status: 500 },
      );
    }
    return NextResponse.json(
      { error: 'An unknown error occurred' },
      { status: 500 },
    );
  }
}
