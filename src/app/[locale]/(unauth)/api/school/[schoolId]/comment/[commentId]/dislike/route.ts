import { NextResponse } from 'next/server';

// Handles POST requests to /api/school/:schoolId/comment/:commentId/dislike
export async function POST(
  request: Request,
  { params }: { params: { schoolId: String; commentId: String } },
) {
  try {
    const { schoolId, commentId } = params;

    // Check if schoolId and commentId exist
    if (!schoolId || !commentId) {
      return NextResponse.json(
        { error: 'schoolId or commentId is missing' },
        { status: 400 },
      );
    }

    const url = new URL(
      `http://127.0.0.1:5000/school/${schoolId}/comment/${commentId}/dislike`,
    );
    url.searchParams.append('timestamp', Date.now().toString());

    // Get the body from the request
    // const body = await request.json();

    const response = await fetch(url.toString(), {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
    });

    // Check if the request was successful
    if (!response.ok) {
      throw new Error('Request failed');
    }

    return NextResponse.json(await response.json());
  } catch (error) {
    if (error instanceof Error) {
      return NextResponse.json(
        { error: `An error occurred: ${error.message}` },
        { status: 500 },
      );
    }
    return NextResponse.json(
      { error: 'An unknown error occurred' },
      { status: 500 },
    );
  }
}
