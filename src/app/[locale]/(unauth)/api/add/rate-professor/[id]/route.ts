import { NextResponse } from 'next/server';

// Handles POST requests to /api/add/rate-professo/:id
export async function POST(
  request: Request,
  {
    params,
  }: {
    params: { id: string };
  },
) {
  try {
    const { id } = params;
    // Check if professorId exists
    if (!id) {
      return NextResponse.json(
        { error: 'Professor id is missing' },
        { status: 400 },
      );
    }

    const requestData = await request.json();

    // Check if body exists
    const requiredProperties = [
      'course',
      'courseOnline',
      'professor',
      'difficulty',
      'takeAgain',
      'takeCredit',
      'needBook',
      'attendance',
      'grade',
      'comment',
    ];

    if (
      !requestData ||
      !requiredProperties.every((prop) => prop in requestData)
    ) {
      return NextResponse.json(
        { error: 'Request body is missing or incomplete' },
        { status: 400 },
      );
    }

    const {
      course,
      courseOnline,
      professor,
      difficulty,
      takeAgain,
      takeCredit,
      needBook,
      attendance,
      grade,
      comment,
    } = requestData;

    const url = new URL(`http://127.0.0.1:5000/rateschool/${id}`);
    url.searchParams.append('timestamp', Date.now().toString());

    const response = await fetch(url.toString(), {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        Pragma: 'no-cache',
        Expires: '0',
      },
      body: JSON.stringify({
        course,
        courseOnline,
        professor,
        difficulty,
        takeAgain,
        takeCredit,
        needBook,
        attendance,
        grade,
        comment,
      }),
    });

    if (!response.ok) {
      throw new Error('Response not OK');
    }

    const responseData = await response.json();

    return NextResponse.json(responseData);
  } catch (error) {
    if (error instanceof SyntaxError) {
      // Handle the specific error here
      console.error('A JSON SyntaxError occurred:', error);
      return NextResponse.json(
        { error: 'Invalid JSON format' },
        { status: 400 },
      );
    }
    if (error instanceof Error) {
      return NextResponse.json(
        { error: `An error occurred: ${error.message}` },
        { status: 500 },
      );
    }
    return NextResponse.json(
      { error: 'An unknown error occurred' },
      { status: 500 },
    );
  }
}
