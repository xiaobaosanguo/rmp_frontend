import { NextResponse } from 'next/server';

// Handles GET requests to /api
export async function GET(
  request: Request,
  { params }: { params: { professorId: String } },
) {
  try {
    const { professorId } = params;

    // Check if id exists
    if (!professorId) {
      return NextResponse.json({ error: 'id is missing' }, { status: 400 });
    }

    const url = new URL(`http://127.0.0.1:5000/professor/${professorId}`);
    url.searchParams.append('timestamp', Date.now().toString());

    /* maybe use a conditional GET request. 
    This involves using ETag and If-None-Match headers. */
    const response = await fetch(url.toString(), {
      headers: {
        'Cache-Control': 'no-cache',
      },
    });

    if (!response.ok) {
      return NextResponse.json(
        { error: `Fetch failed with status: ${response.status}` },
        { status: response.status },
      );
    }

    const data = await response.json(); // Parse the response to JSON

    return NextResponse.json({ data });
  } catch (error) {
    if (error instanceof Error) {
      return NextResponse.json(
        { error: `An error occurred: ${error.message}` },
        { status: 500 },
      );
    }
    return NextResponse.json(
      { error: 'An unknown error occurred' },
      { status: 500 },
    );
  }
}
