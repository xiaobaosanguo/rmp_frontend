import { render, screen } from '@testing-library/react';

import { AppConfig } from '@/utils/AppConfig';

import { BaseTemplate } from './BaseTemplate';

describe('BaseTemplate', () => {
  test('renders without crashing', () => {
    render(
      <BaseTemplate>
        <div />
      </BaseTemplate>,
    );
    const linkElement = screen.getByText(/© Copyright/i);
    expect(linkElement).toBeInTheDocument();
  });

  test('displays the correct copyright information', () => {
    render(
      <BaseTemplate>
        <div />
      </BaseTemplate>,
    );
    const currentYear = new Date().getFullYear();
    const copyrightText = `© Copyright ${currentYear} ${AppConfig.name}.`;
    expect(screen.getByText(copyrightText)).toBeInTheDocument();
  });
});
