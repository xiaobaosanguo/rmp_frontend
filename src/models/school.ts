export interface Comment {
  comment_id: number;
  attendance: string;
  course_title: string;
  credit: number;
  date: string;
  difficulty: string;
  grade: string;
  rating: string;
  reselect: string;
  review: string;
  textbook: string;
  thumbDown: number;
  thumbUp: number;
}

export interface SchoolInfo {
  abbr_name: string;
  city: string;
  cn_name: string;
  comments: Comment[];
  commentsTotal: number;
  country: string;
  name: string;
  province: string;
  rating: number;
  ratingTotal: number;
  website: string;
}

export interface School {
  id: number;
  schoolInfo: SchoolInfo;
}
