type UUID = number;
type DateTime = string;

interface Comment {
  comment_id: number;
  course_title: string;
  rating: number;
  difficulty: number;
  credit: number;
  attendance: string;
  textbook: string;
  grade: string;
  reselect: string;
  review: string;
  date: DateTime;
  thumbUp: number;
  thumbDown: number;
}

interface ProfessorInfo {
  firstName: string;
  lastName: string;
  department: string;
  department_cn: string;
  schoolId: UUID;
  schoolName: string;
  schoolName_cn: string;
  rating: number;
  ratingTotal: number;
  reselectPercentage: number;
  difficulty: number;
  commentsTotal: number;
  courses: string[];
  comments: Comment[];
}

export interface Professor {
  id: UUID;
  professorInfo: ProfessorInfo;
}
