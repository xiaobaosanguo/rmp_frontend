import type { Config } from 'tailwindcss';

export default {
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
  purge: {
    options: {
      safelist: ['rounded-b-none'],
    },
    // ...
  },
  theme: {
    fontSize: {
      xs: '0.75rem',
      sm: '0.875rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
    },
    fontWeight: {
      thin: '100',
      extralight: '200',
      light: '300',
      normal: '400',
      medium: '500',
      semibold: '600',
      bold: '700',
      extrabold: '800',
      black: '900',
    },
    extend: {
      dropShadow: {
        '3xl': '0 35px 35px rgba(0, 0, 0, 0.25)',
        '4xl': [
          '0 35px 35px rgba(0, 0, 0, 0.25)',
          '0 45px 65px rgba(0, 0, 0, 0.15)',
        ],
        logo: '0px 2px 1px rgba(0, 0, 0, 0.25)',
      },
      colors: {
        gray: {
          100: '#f7fafc',
          200: '#edf2f7',
          300: '#e2e8f0',
          400: '#cbd5e0',
          500: '#a0aec0',
          600: '#718096',
          700: '#4a5568',
          800: '#2d3748',
          900: '#1a202c',
        },
        blue: {
          100: '#ebf8ff',
          200: '#bee3f8',
          300: '#90cdf4',
          400: '#63b3ed',
          500: '#4299e1',
          600: '#3182ce',
          700: '#2b6cb0',
          800: '#2c5282',
          900: '#2a4365',
        },
        green: {
          50: '#F2FFF6',
          100: '#89FFD0',
          200: '#37775D',
          300: '#1B5734',
          400: '#0A6724',
          500: '#74BA87',
          600: '#99D815',
          700: '#5BB449',
          800: '#BDCCBA',
          900: '#00C777',
          1000: '#8CE5A4',
          1100: '#0A6423',
          1200: '#00AA2F',
          1300: '#0E432D',
        },
        yellow: {
          100: '#FFD700',
          200: '#FFD700',
          300: '#FEB213',
          400: '#FFAD00',
          500: '#CC8C00',
          600: '#E69B00',
          700: '#FDC432',
        },
        red: {
          100: '#C7261A',
          200: '#F83225',
        },
        orange: {
          100: '#C55F1E',
          200: '#F87125',
        },
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-linear': 'linear-gradient(var(--tw-gradient-stops))',
        'gradient-conic': 'conic-gradient(var(--tw-gradient-stops))',
      },
    },
  },
  variants: {
    extend: {
      backgroundImage: ['hover', 'focus'],
    },
  },
  plugins: [],
} satisfies Config;
